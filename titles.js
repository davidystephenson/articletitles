const cheerio = require('cheerio')
const babyparse = require('babyparse')
const request = require('sync-request')
const fs = require('fs')

const getSlug = url => {
  const index = url.lastIndexOf('/')
  return url.substr(index + 1)
}

const slugify = str => {
  str = str.replace(/^\s+|\s+$/g, '') // trim
  str = str.toLowerCase()

  // remove accents, swap ñ for n, etc
  var from = 'ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;'
  var to = 'aaaaaeeeeeiiiiooooouuuunc------'
  for (var i = 0, l = from.length; i < l; i++) {
    str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i))
  }

  str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
    .replace(/\s+/g, '-') // collapse whitespace and replace by -
    .replace(/-+/g, '-') // collapse dashes

  return str
}

const data = babyparse.parseFiles('urls.csv').data

const output = JSON.parse(JSON.stringify(data))

const getNewSlug = datum => {
  if (datum.length > 1) {
    const response = request('GET', `http://${datum[0]}`)

    const $ = cheerio.load(response.body)
    const title = $('.Page-info-title').text()

    const badSlug = getSlug(datum[1])
    const goodSlug = slugify(title)

    if (badSlug === goodSlug) {
      if (goodSlug.indexOf('-') > -1) {
        const index = goodSlug.lastIndexOf('-')
        return goodSlug.slice(0, index)
      } else {
        const category = $('.Page-info-type a').text().toLowerCase()
        if (category) {
          return `${goodSlug}-${category}`
        } else {
          return null
        }
      }
    } else {
      return goodSlug
    }
  }
}

data.map((datum, index) => {
  console.log('Processing:', datum[0], `(${index})`)
  const slug = getNewSlug(datum)
  if (slug) {
    output[index][2] = `http://www.healthcentral.com/article/${slug}`
  } else {
    output[index][2] = 'Invalid URL.'
  }
})

const csv = output.map(row => row.join(','))
const text = csv.join('\n')
console.log('text test:', text)

fs.writeFileSync('output.csv', text, 'utf8')
